sudo zypper ref 
sudo zypper up -y
sudo zypper dup -y

# Add Repositorys Pacman,VLC,DBEAVER
sudo zypper ar -cfp 90 'https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Leap_$releasever/' packman
sudo zypper ar -cfp 90 'http://download.videolan.org/pub/vlc/SuSE/Leap_15.3/' vlc
sudo zypper ar -cfp 90 'https://download.opensuse.org/repositories/home:/Dead_Mozay/openSUSE_Leap_$releasever/' dbeaver

# Tumbleweed
sudo zypper ar -cfp 90 'https://download.opensuse.org/repositories/home:/Dead_Mozay/openSUSE_Tumbleweed/' dbeaver


sudo zypper ref 
sudo zypper up -y
sudo zypper dup -y

sudo zypper in -t pattern devel_basis
sudo zypper in git tilix zlib-devel bzip2 libbz2-devel libffi-devel libopenssl-devel readline-devel sqlite3 sqlite3-devel xz xz-devel\
tilix python3-virtualenv python-virtualenvwrapper zsh dbeaver -y

pip3 install -U pip
pip3 install virtualenvwrapper
pip3 install ipython[all]
    
sudo chown $USER:users -R /opt
wget -c https://bitbucket.org/wt3c/after_install_user/raw/4048ceacc6befe6c401b86d1a2611a99abd336fb/install/jdk-8u241-linux-x64.tar.gz -P /opt/
wget -c https://bitbucket.org/wt3c/after_install_user/raw/4048ceacc6befe6c401b86d1a2611a99abd336fb/install/spark-3.0.1-bin-hadoop3.2.tgz -P /opt/

mkdir /opt/{java,spark}
tar -xzvf /opt/jdk-8u241-linux-x64.tar.gz  --directory=/opt/java/ --strip 1
tar -xzvf /opt/spark-3.0.1-bin-hadoop3.2.tgz --directory=/opt/spark --strip 1

sudo chown -R $USER:users /opt/

profile='# ########## HADOOP e CIA #################### #
                                                                       
# ########## JAVA
JAVA_HOME=/opt/java
JAVA_JRE=$JAVA_HOME/jre
#
# ######### PYENV
alias manage="python $VIRTUAL_ENV/manage.py"
export PYTHONPATH=$VIRTUAL_ENV
source /usr/bin/virtualenvwrapper
export WORKON_HOME=$HOME/workspace/

export PYENV_ROOT="/opt/pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

if command -v pyenv 1>/dev/null 2>&1; then
   eval "$(pyenv init -)"
fi

#                                                                                                                       
# ####### SPARK                                                                                                         
export SPARK_HOME=/opt/spark
export SPARK_CONF_DIR=/opt/spark/conf
export SPARK_MASTER_HOST=namenode
export PYSPARK_PYTHON=/opt/pyenv/versions/3.6.12/bin/python # SET VERSION PYTHON
export PYSPARK_DRIVER_PYTHON=ipython
# export PYSPARK_DRIVER_PYTHON_OPTS="notebook"                                                                                                                                                                                              
# export HIVE_AUX_JARS_PATH=$(find /opt/spark/jars/ -name "*.jar" -print0 | sed "s/\x0/,/g")
                                                                                                                        
alias pyspark=$SPARK_HOME/bin/pyspark'

echo "%%%%%%%%%%%%%%%%%%%% profile %%%%%%%%%%%%%%%%%%%%%%%%%%"
# sudo echo -e "$profile" >> /etc/profile
# source /etc/profile

# #Configuração JAVA
sudo alternatives --install /usr/bin/java java /opt/java/bin/java 2
sudo alternatives --config java
sudo alternatives --install /usr/bin/jar jar /opt/java/bin/jar 2
sudo alternatives --install /usr/bin/javac javac /opt/java/bin/javac 2
sudo alternatives --set jar /opt/java/bin/jar
sudo alternatives --set javac /opt/java/bin/javac

# Pyenv
git clone https://github.com/pyenv/pyenv.git /opt/pyenv
source /etc/profile

pyenv install 3.6.12
## Opcional
pyenv global 3.6.12

#Geração das chaves - SSH
ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa
#ssh-copy-id -i ~/.ssh/id_rsa root@ip_do_mesmo_server
eval `ssh-agent -s`
ssh-add ~/.ssh/id_rsa



# Configurando os Hosts
hosts_my="192.168.0.248   namenode
192.168.0.247    master
192.168.0.242    sparkdsv"

sudo  echo -e "$hosts_my" >> /etc/hosts


# Configuração VIM
echo "######### VIM ################" 
# git clone git@github.com:wt3c/myvim.git ~/
wget -c https://github.com/wt3c/myvim/archive/master.zip
unzip master.zip -d ~/

mv ~/myvim-master ~/.vim
ln -s ~/.vim/.vimrc ~/.vimrc



# sudo sh -c "$(curl -fsSL https://bitbucket.org/wt3c/after_install_user/raw/master/after/after_install_user.sh)"
